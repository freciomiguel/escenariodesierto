using System.Collections;
using System.Collections.Generic;
using UnityEngine;


  [RequireComponent(typeof(CharacterController))]
  [RequireComponent(typeof(Animator))]
public class CharacterAnimBasedMovement : MonoBehaviour
{
    // Start is called before the first frame update
    public float rotationSpeed = 4f;
    public float rotationThreshold = 0.3f;
    [Range(0, 180f)]
    public float degreesToTurn = 160f;

    [Header("Animator Parameters")]
    public string motionParam = "motion";
    public string mirrorIdleParam = "mirrorIdle";
    public string turn180Param = "turn180";
    public string dashParam = "Dash";
    public string jumpParam = "Jump";

    [Header("Animation Smoothing")]
    [Range(0, 1f)]
    public float StartAnimTime = 0.3f;
    [Range(0, 1f)]
    public float StopAnimTime = 0.05f;

    private Ray wallRay = new Ray();
    private float Speed;
    private Vector3 desiredMoveDirection;
    private CharacterController characterController;
    private Animator animator;
    private bool mirrorIdle;
    private bool turn180;
    private bool dash;
    private bool jump;

    void Start()
    {
        characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
    }

    private void OnAnimatorIK(int layerIndex)
    {

        if (Speed < rotationThreshold) return;

        float distanceToLeftFoot = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.LeftFoot));

        float distanceToRightFoot = Vector3.Distance(transform.position, animator.GetIKPosition(AvatarIKGoal.RightFoot));

        // Right foot in front

        if (distanceToRightFoot > distanceToLeftFoot)
        {

            mirrorIdle = true;

            // Right foot behind

        }
        else
        {

            mirrorIdle = false;
        }
    }
    public void moveCharacter(float hInput, float vInput, Camera cam, bool jump, bool dash)
    {
        //Calculate the Input Magnitude
        Speed = new Vector2(hInput, vInput).normalized.sqrMagnitude;

        // Check if dash button is pressed
        if (dash)
        {
            animator.SetBool(dashParam, true);
        }
        else
        {
            animator.SetBool(dashParam, false);
        }

        if (jump)
        {
            animator.SetBool(jumpParam, true);
        }
        else
        {
            animator.SetBool(jumpParam, false);
        }


        //Physically move player
        if (Speed > rotationThreshold)
        {
            animator.SetFloat(motionParam, Speed, StartAnimTime, Time.deltaTime);
            Vector3 forward = cam.transform.forward;
            Vector3 right = cam.transform.right;
            forward.y = 0f;
            right.y = 0f;
            forward.Normalize();
            right.Normalize();
            // Rotate the character towards desired move direction based on player input and camera position 
            desiredMoveDirection = forward * vInput + right * hInput;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredMoveDirection), rotationSpeed * Time.deltaTime);
        }

        else if (Speed < rotationThreshold)
        {
            animator.SetBool(mirrorIdleParam, mirrorIdle);
            animator.SetFloat(motionParam, Speed, StopAnimTime, Time.deltaTime);
        }
    }
}
