using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Playables;
[RequireComponent(typeof(Collider))]
public class TimelineActivator : MonoBehaviour
{

    private void Update()
    {
        if (playerInside && !isPlaying)
        {
            if (interact || autoActivate)
            {
                PlayTimeline();
            }
        }
    }
    public PlayableDirector playableDirector;
    public string playerTAG;
    public Transform interactionLocation;
    public bool autoActivate = false;
    // Reference to the PlayableDirector component that contains the Timeline
    // Tag to check against when some object enter or exit the trigger
    // Reference to the transform where the player will be placed when the cinematic starts // Will be the cinematic activated when an object with a valid TAG enters the trigger.
    public bool interact { get; set; }
    [Header("Activation Zone Events")] public UnityEvent OnPlayerEnter; public UnityEvent OnPlayerExit;
    // Property activated from outside this object that behaves like an input button.
    // Events to raise when an object with a valid Tag enters the trigger. // Events to raise when an object with a valid Tag exits the trigger.
    [Header("Timeline Events")]
    public UnityEvent OnTimeLineStart; public UnityEvent OnTimeLineEnd;
    private bool isPlaying; private bool playerInside;
    private Transform playerTransform;

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals(playerTAG))
        {
            playerInside = true;
            playerTransform = other.transform; OnPlayerEnter.Invoke();
        }
    }


    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag.Equals(playerTAG))
        {
            playerInside = false; playerTransform = null; OnPlayerExit.Invoke();
        }
    }



    private IEnumerator waitForTimeLineToEnd()
    {
        // Invoke the methods linked to // the beginning of the cinematic
        OnTimeLineStart.Invoke();
        // Get the duration of the timeline from the playable Director
        float timeLineDuration = (float)playableDirector.duration;

        while (timeLineDuration > 0)
        {
            timeLineDuration -= Time.deltaTime;
            yield return null;
        }// Reset variable
        isPlaying = false;
        // Invoke the methods linked to
        // the end of the cinematic
        OnTimeLineEnd.Invoke();

    }
        private void PlayTimeline() 
    { 
    
        // Place the character at the correct interaction position
        if (playerTransform && interactionLocation)
            playerTransform.SetPositionAndRotation(interactionLocation.position, interactionLocation.rotation);
        // Avoid infinite interaction loop
        if (autoActivate)
            playerInside = false;
        // Play de Timeline
        if (playableDirector)
            playableDirector.Play();

        // Set Variables
        isPlaying = true;
        interact = false;
        // Wait for Timeline to end
        StartCoroutine(waitForTimeLineToEnd());
    }


}
// Events to raise when the Timeline starts playing. // Events to raise when the Timeline stops playing.
// Determines if the Timeline is currently playing
// Determines if the player is inside of the trigger or not // Reference to the player Transform 
